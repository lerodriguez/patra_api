```php
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Run Application with Docker Container locally
 * Local Deployment Without Docker Container
 * AWS deployment from local host
 * Automatic Deployments with BitBucket
```

INTRODUCTION
------------

This project is a challenge to test DevOps skill in a real Project, the stack includes:

 - Python API with a Endpoint responding to HTTP GET request.
 - Terraform templates to deploy all infrastructure as IaC.
 - Automated deployments using Bitbucket Pipelines.
 - Deployment in ECS with autoscaling.


REQUIREMENTS
------------
This project runs in Python >= `3`
We recommend to use `virtualenv` for development:

- Start by installing `virtualenv` if you don't have it
```
pip install virtualenv
```
- Create a virtual environment
```
virtualenv env
```
- Enable the virtual environment
```
source env/bin/activate
```

- Install the python dependencies on the virtual environment
```
pip install -r requirements.txt
```



## Run Application  with Docker Container locally:
This project can run in a docker container  in your local computer.

- Clone repository:

```
git clone https://lerodriguez@bitbucket.org/lerodriguez/patra_api.git
```

- Build the docker image of the application, execute this command in the app folder `patra_api`

 ```
 docker build . -t python_app:latest
 ```

- Run a docker container  of the application:

 ```
 docker run -it -p 8080:8080 python_app:latest
 ```

- Open your browser to see the app healthCheck page:
```
http://localhost:8080
```
- To query the API endpoint:
```
http://127.0.0.1:8080/api/v1/resources/books/all
http://127.0.0.1:5000/api/v1/resources/books?id=1
```
Local Deployment Without Docker Container
------------
No Environment Variables are needed for local deployment

- Run the API
```
gunicorn -w 1 -b 0.0.0.0:8080 api:app
```

AWS deployment from local host
------------
*Please make sure to setup your AWS account credentials before continuing with these steps.* [AWS CLI CONFIG](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)
To run all the infra you need to move to the infra repository [patra_terraform](https://bitbucket.org/lerodriguez/patra_terraform/src/master/)
- Enter the infra Folder
```
cd build_ecs_infra
```
- Run terraform init

```
terraform init --backend-config=backend.hcl --reconfigure
```

- Run terraform plan **( optional )**
```
terraform plan -var="region=<region>" -var="stack_id=<desired_stack_name>" -var="env=<dec/prod/qa>" -var="commit_id=<docker_image_tag>"
```

- Run terraform apply
```
terraform apply -var="region=<region>" -var="stack_id=<desired_stack_name>" -var="env=<dec/prod/qa>" -var="commit_id=<docker_image_tag>"
```

Automatic Deployments with BitBucket
------------

Variables Required to be setup in bitbucket account:
------------
| Name | Description |  Required |
|------|-------------|:----:|:-----:|:-----:|
| AWS_ACCESS_KEY_ID | AWS Access Key | yes |
| AWS_SECRET_ACCESS_KEY | AWS Secret Key | yes |
| AWS_DEFAULT_REGION | AWS Region to deploy the stack | yes |
| BITBUCKET_USERNAME | Bitbucket username to call terraform pipeline | yes |
| BITBUCKET_APP_PASSWORD | Bitbucket app password (this is not the account password) | yes |
| STACK_ID | Name for the ECS stack | yes |
| ENV | Environment name (DEV/QA/PROD) | yes |

This project is parameterized and will run automatically with every push to the `patra_api` master branch repository.

Infraestructure Deployment WorkFlow
-
![Infraestructure Deployment Workflow](https://lewisrodriguez04.s3.us-east-2.amazonaws.com/InfraWorkflowblue.png)

Infraestructure
-
![enter image description here](https://lewisrodriguez04.s3.us-east-2.amazonaws.com/Aplication_Infraestructure.png)
