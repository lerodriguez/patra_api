output "s3-state-base-infra-bucket" {
	value = aws_s3_bucket.states_bucket.bucket_domain_name 
}

output "s3-state-base-infra-bucketi-id" {
	value = aws_s3_bucket.states_bucket.id
}
