resource "aws_s3_bucket" "states_bucket" {
  bucket = "python-app-state-bucket"
  acl    = "private"

  tags = {
    Name        = "State Files"
    Environment = var.region
  }
}

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.states_bucket.id
  key    = "base_infraestructure/terraform.tfstate"
  acl    = "private"
  source = ".terraform/terraform.tfstate"
  depends_on = [
	aws_s3_bucket.states_bucket, aws_ecr_repository.python_app
	]
}
