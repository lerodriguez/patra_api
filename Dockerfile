FROM python:3
WORKDIR /opt/python-app
COPY . .
RUN pip3 install -r requirements.txt --no-cache-dir --no-warn-script-location
EXPOSE 8080
CMD ["gunicorn","-w","1","-b","0.0.0.0:8080","api:app"]
